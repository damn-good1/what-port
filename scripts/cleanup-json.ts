import { fstat, writeFileSync } from "fs"

const path = require("path")

type Protocol = "tcp" | "udp" | "sctp" | "dccp"

type ServiceBase = {
    name: string,
    description: string,
    assignee: string,
    contact: string,
    reference: string,
    serviceCode: string,
    unauthorizedUseReported: string,
    assignmentNotes: string,
}

type ImportedService = ServiceBase & {
    port: string,
    registrationDate: string,
    modificationDate: string,
    protocol: Protocol | "",
}

type Service = ServiceBase & {
    port: number,
    registrationDate: Date,
    modificationDate: Date,
    protocol: Protocol,
}

type CleanedService = {
    name: string,
    description: string,
    assignee: string,
    contact: string,
    reference: string,
    serviceCode: string,
    unauthorizedUseReported: string,
    assignmentNotes: string,
    ports: { [key: string]: number[] | number }
    registrationDate: Date,
    modificationDate: Date,
    protocols: Protocol[],
}

const HERE = path.resolve(__dirname);
const PROJD_DIR = path.dirname(HERE);
const ASSETS = path.resolve(PROJD_DIR, "src", "assets");
const RECOLED = path.resolve(ASSETS, "iana-service-names-port-numbers-recoled.json");

const services: ImportedService[] = require(RECOLED)

const keys = services.map(service => service.name).filter((v, i, a) => a.indexOf(v) == i).map(serviceName => serviceName.toLowerCase());

function uniq(arr: any[]) {
    return arr.filter((v, i, a) => a.indexOf(v) == i);
}

const cleaned = keys.map((key: string) => {
    const copies = services.filter(service => service.name.toLocaleLowerCase() == key);
    const first = copies[0];
    return {
        description: first.description,
        assignee: first.assignee,
        contact: first.contact,
        reference: first.reference,
        serviceCode: first.serviceCode,
        unauthorizedUseReported: first.unauthorizedUseReported,
        assignmentNotes: first.assignmentNotes,
        registrationDate: new Date(first.registrationDate),
        modificationDate: new Date(first.modificationDate),
        name: key,
        ports: Object.fromEntries(copies.map(s => {
            return [s.protocol,
            s.port.match(/\d+\-\d_/) ? s.port.split("-").map(parseInt) : parseInt(s.port)
            ]
        })),
    } as CleanedService
})

const OUTPATH = RECOLED.replace(/\.json$/, "-cleaned.json");

writeFileSync(OUTPATH, JSON.stringify(cleaned))