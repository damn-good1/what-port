const path = require("path");
const { stdout } = require("process");

const HERE = path.resolve(__dirname);
const PROJD_DIR = path.dirname(HERE);
const SOURCE = path.resolve(
  PROJD_DIR,
  "src",
  "assets",
  "iana-service-names-port-numbers-recoled-cleaned.json"
);

function main() {
  const SERVICES: any[] = require(SOURCE);
  stdout.write("<template>\n");
  SERVICES.forEach((service) => {
    stdout.write(
      `<h1>What the default port of ${service.name} (${service.description})?</h1>\n`
    );
  });
  stdout.write("</template>\n");
}

main();
