import { Proto } from "./protocol";
import importedServices from "../assets/iana-service-names-port-numbers-recoled-cleaned.json"
export type Protocol = "tcp" | "udp" | "sctp" | "dccp"

type ServiceBase = {
    name: string,
    description: string,
    assignee: string,
    contact: string,
    reference: string,
    serviceCode: string,
    unauthorizedUseReported: string,
    assignmentNotes: string,
    ports: { [index: string]: number | number[] },
}

type ImportedService = ServiceBase & {
    registrationDate: string,
    modificationDate: string,
    protocols: (Protocol | "")[],
}

export type Service = ServiceBase & {
    registrationDate: Date,
    modificationDate: Date,
}

export function loadServices(): Service[] {
    const services: ImportedService[] = importedServices as any;

    return services.map((service) => {
        return {
            ...service,
            registrationDate: new Date(service.registrationDate),
            modificationDate: new Date(service.modificationDate),
        } as Service;
    })
}